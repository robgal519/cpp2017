#pragma once
#include <iostream>     // std::cout
#include <iterator>     // std::ostream_iterator
#include <list>
#include <algorithm>
#include <numeric> 
#include <cmath>

class Stat
{
	
public:
	std::list<float> data_;
	//serves as input for data;
	Stat& operator()(float data){data_.push_back(data); return *this;}
	//deletes maximum element and minimum from set
	void dropMinMax();
	// printing all data like [ data data data]
	friend std::ostream&  operator<<(std::ostream& out, Stat& a);
	//return average of set
	float average();
	//return variance of set
	float variance();
	//return median of set
	float median();
	// return min of set
	float min();
	// return max of set
	float max();
};

