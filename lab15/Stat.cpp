#include "Stat.h"
std::ostream&  operator<<(std::ostream& out, Stat& a){
	 std::ostream_iterator<float> out_it (std::cout," ");
	 out << '[';
	 std::copy ( a.data_.begin(), a.data_.end(), out_it );
	 out <<']';
return out;
}
void Stat::dropMinMax(){
		auto result = std::minmax_element (data_.begin(),data_.end());
		data_.erase(result.first);
		data_.erase(result.second);
	}
float Stat::average(){
	float sum = std::accumulate(data_.begin(), data_.end(),0.0);
	return sum/data_.size();
}
float Stat::variance(){
		float sumsq = std::accumulate(data_.begin(), data_.end(),0.0,[this](float sum ,float y){
			return sum + y*y;
		});

		return sqrt(sumsq/data_.size()-average()*average());
	}
float Stat::median(){
		data_.sort();
		std::list<float>::iterator it = data_.begin();
		std::advance (it,data_.size()/2);
		return *it;
	}
float Stat::min(){
		auto result	=std::minmax_element (data_.begin(),data_.end());
		return *result.first;
	}

float Stat::max(){
		auto result	=std::minmax_element (data_.begin(),data_.end());
		return *result.second;
	}