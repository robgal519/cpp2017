#pragma once
#include <deque>
#include <string>
#include <iostream>

class Person
{
	std::string name_;
 	char sex_; //M means men, K women
public:
	Person(std::string name, char sex = 'M'):name_(name),sex_(sex){}
	static Person kobieta(std::string name);
	~Person(){};
	operator std::string()const; // formats the data to string as : Name sex, required for QKolejka::prosze(void)
	
	char getSex()const{ //simple getter required for QKolejka::prosze
		return sex_;
	}
};

class QKolejka
{
	std::deque<std::string> que_;
public:
	//need to be defined for convenience, and becouse it was used in main function
	typedef std::deque<std::string>::const_iterator const_iterator;
	void prosze(Person person);

	//simple iterators to begin and end, for easy acces, for eficiency left inline
	const_iterator begin()const{
		return que_.begin();
	}
	const_iterator end()const{
		return que_.end();
	}
	//pop data ( women first ) 
	std::string prosze(void);
	//checkicg if there are any data stored, returns true if it stores data, false if list is empty
	operator bool()const{//very simple so i left it inline
		return !que_.empty();
	}
};

