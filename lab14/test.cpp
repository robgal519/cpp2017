

/* Zdefiniowana jest klasa Person, ktora rozroznia plec ( posiada skladnik bool rozrozniajacy ).
   
   Zadana jest tez Kulturalna Koleja, kolejka ta puszcza przodem kobiety.
   
   UWAGA: Mozna uzyc specjalnego kontenera a mozna saortowac samodzielnie.

   
   
 */ 

#include "QKolejka.h"

int main() {
  QKolejka k;
  k.prosze( Person::kobieta( "Anna" ) );
  k.prosze( Person( "Janek" ) );
  k.prosze( Person::kobieta( "Ewa" ) );
  k.prosze( Person::kobieta( "Kasia" ) );
  k.prosze( Person::kobieta( "Aisha" ) );
  k.prosze( Person( "Grzegorz" ) );
  k.prosze( Person( "Tomasz" ) );
  k.prosze( Person::kobieta( "Stanislawa" ) );

  for ( QKolejka::const_iterator i = k.begin(); i != k.end(); ++i ) {
    std::cout << *i << " " << std::endl;
  }
  
  k.prosze( Person( "Rysiek" ) );
  k.prosze( Person::kobieta( "Iza" ) );
  
  while ( k ) {
    std::cout << k.prosze(  ) << " ";
  }
  
}
/* wynik
Anna K 
Kasia K 
Ewa K 
Stanislawa K 
Aisha K 
Grzegorz M 
Tomasz M 
Janek M 
Anna K Iza K Ewa K Kasia K Aisha K Stanislawa K Janek M Tomasz M Rysiek M Grzegorz M 

 */
