#pragma once

class Punkt{
int coordinates_[2];

public:
	//Konstruktor, zapisuje współrzędne
	Punkt(int x, int y);

template<int Id> //Funkcja umożliwia odwołanie so współrzędnych. Jako typ do template należy podać nr. współrzędnej do której chcemy się odwołać ( indeksowane od 0)
	int wsp()const;

template<typename T> // Funkcja zwraca minimum z dwóch dowolnych wartości, dla typów definiowanych przez użytkownika konieczny jest operator < zwracający wartości (true/false)
	static T min(T x1,T x2);

template<typename T> //Podobnie do funkcji min, typy definiowane przez użytkownika wymagają operatora <
	static T max(T x1, T x2);

//operator konieczny do porównywania obiektów za pomocą funkcji min i max
bool operator<(const Punkt& p)const;

};
#include "Punkt.inl"