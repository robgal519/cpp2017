template<int Id>
int Punkt::wsp()const{
	if(Id<2)
		return coordinates_[Id];
	else
		throw -1;
}

template<typename T>
T Punkt::min(T x1,T x2){
	if(x1<x2)
		return x1;
	else 
		return x2;
}

template<typename T>
T Punkt::max(T x1, T x2){
	if(x1<x2)
		return x2;
	else 
		return x1;
}