#pragma once
#include <string>
#include <cstdio>

//konterner na dane
struct Rozdzielczosc
{
	int x_,y_;

	Rozdzielczosc(int x, int y):x_(x),y_(y){}
	Rozdzielczosc():x_(0),y_(0){}

	std::string getXY()const;

};
//wszystkie klasy dziedziczą po urządzeniu, klasa bazowa
class Urzadzenie
{
public:
	virtual std::string rozdzielczosc()const=0;
	virtual std::string rodzaj() const =0;

};

// Papierożerne jest typen urządzenia
class Papierozerne:public Urzadzenie
{
};

//drukarka może być papierożerna, ale inne urządzenia też mogą takie być więc musi być wirtualne bo jeśli zrobimy klasę dziedziczącą po urządzeniach, to będzei problem bez wirtualnego dziedziczenia
class Drukarka:virtual public Papierozerne
{
	Rozdzielczosc rozdzielczosc_;
public:
	Drukarka(Rozdzielczosc r1):rozdzielczosc_(r1){}

	virtual std::string rozdzielczosc()const ;
	virtual std::string rodzaj() const;
};
//skaner jest urządzeniem
class Skaner: public Urzadzenie
{
	Rozdzielczosc rozdzielczosc_;
public:
	Skaner(Rozdzielczosc r1):rozdzielczosc_(r1){}

	virtual std::string rozdzielczosc()const ;
	virtual std::string rodzaj() const;
};

//ksero jest papierożerne
class Ksero:virtual public Papierozerne
{
public:

	virtual std::string rodzaj() const;
};
//wielofunkcyjne jest połączeniem wszystkich urządzeń, więc musi dziedziczyć po wszystkich
class Wielofunkcyjne: public Drukarka, public Skaner, public Ksero
{
public: 
	Wielofunkcyjne(Rozdzielczosc r1, Rozdzielczosc r2):Drukarka(r1),Skaner(r2){};
	virtual std::string rodzaj() const;
};
