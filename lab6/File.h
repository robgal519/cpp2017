#pragma once

class File
{
public:
	
	std::string name_;
	unsigned indent;

	File(std::string name)
	{
		indent = 0;
	}
	~File();

	
	std::ostream virtual print()const ;
	friend std::ostream operator << (std::ostream& cout, const File& f);
};

std::ostream operator << (std::ostream& cout,const File& f)
{
	cout<<f.print();
}