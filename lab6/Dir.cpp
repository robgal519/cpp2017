#include "Dir.h"

void Dir::operator += (File* f)
{
	children.push_back(f);
	children[children.size()-1]->indent = indent+2;
}
void Dir::operator -= (std::string str)
{
	for(int i=0;i<children.size();++i)
		if(children[i]->name_ == str)
		{
			delete children[i];
			children[i] = NULL;
		}
}

std::ostream Dir::print()const 
{
	std::ostream out;
	for(int i=0;i<indent;++i)
		out<<" ";
	out<<name_<<"\n";

	for(int i=0; i<children.size();++i)
	{
		if(children[i])
			out<<children[i]->print();
	}
	return out ;
}
File* Dir::get(std::string str)
{
	for(int i=0;i<children.size();++i)
		if(children[i]->name_ == str)
		{
			return children[i];
		}
	return NULL;
}
void Dir::copy(File* f)
{

}