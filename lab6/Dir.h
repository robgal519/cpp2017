#pragma once
#include "File.h"

class Dir:public File
{
	std::vector<File*> children;
public:
	Dir(std::string name):File(name){}
	~Dir();
	operator std::string();
	void operator+=(*File file);
	void operator-=(std::string name);
	File get(std::string name);
	void copy(File copy);

};

