#pragma once
#include <string>
#include <vector>
#include <iostream>

struct parameters
{
	std::string name;
	float value;

	parameters(std::string n,float v):
	name(n),
	value(v)
	{}
};

class FitResult
{
	std::string equation;
	std::vector<parameters> param;
public:
	FitResult(std::string eq, std::vector<parameters> p):
	equation(eq),
	param(p)
	{};

	std::string expression()const;
	unsigned int nParams()const{return param.size();} //simple so inline
	std::string parameterName(int id) const;
	float parameterValue(int id) const;
};


class Fit
{
	std::string name;
public:
	Fit(std::string n=""):name(n){};
	virtual void appendPoint(float x, float y);
	virtual	FitResult result() const;
	void print()const;
	void appendPoint(float x, float y)const;

};


void Fit::print()const
{
	std::cout << "This is " << name << std::endl;

}

void Fit::appendPoint(float x, float y)const
{
	std::cout<<"Cannot add point ("<<x<<","<<y<<", to const object.\n";
}