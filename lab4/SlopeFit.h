#pragma once
#include "Fit.h"
#include <string>


class SlopeFit : public Fit
{
	float sumXY;
	float sumXX;
	float a;

public:
	SlopeFit(std::string n=""):Fit(n)
	{
		sumXY = 0;
		sumXX = 0;
	}
	virtual void appendPoint(float x, float y);
	virtual	FitResult result() const;


};