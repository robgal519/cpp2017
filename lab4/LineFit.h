#pragma once
#include "SlopeFit.h"

class LineFit : public SlopeFit
{
 float sumX;
 float sumY; 
 unsigned n;
 float b;
public:
	LineFit(std::string p=""):SlopeFit(p)
	{
		sumX=0;
 		sumY=0; 
 		n=0;
		
	}
	virtual void appendPoint(float x, float y);
	virtual	FitResult result() const;

};