#include "LineFit.h"


void LineFit::appendPoint(float x, float y)
{
	sumXY += x*y;
	sumXX += x*x;
	sumX += x;
	sumY += y;
	n += 1;
	a=(sumXy-sumX*sumY/n)/(sumXX-sumX*sumX/n);
	b=(sumY-a*sumX)/n;
}
FitResult LineFit::result() const
{
	std::vector<parameters> p;
	p.push_back(parameters("a",a));
	p.push_back(parameters("b",b));	

	return FitResult("y = a * x + b", p);

} 