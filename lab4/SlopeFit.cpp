#include "SlopeFit.h"
#include "Fit.h"
void SlopeFit::appendPoint(float x, float y)
{
	sumXY += x*y;
	sumXX += x*x; 
	a = sumXY/sumXX;
}

FitResult SlopeFit::result() const
{
	std::vector<parameters> p;
	p.push_back(parameters("a",a));

	return FitResult("y = a * x", p);

}