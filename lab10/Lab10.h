#pragma once

#include <vector>
#include <string>
#include <iostream>

class VariableBase
{
public:
	virtual void Print()const=0;
	VariableBase(){}
	virtual ~VariableBase(){}
	
};

template <typename T >
class Variable: public VariableBase
{
protected:
	T value;
	std::string name;
public:
	void SetValue(T val){value = val;}
	virtual void Print()const;
	Variable(std::string name, T val):value(val), name(name){}
	~Variable(){}
	
};

template <typename T>
class VariableWithUnits: public Variable<T>
{
protected:
	std::string unit;
public:
	virtual	void Print()const;
	VariableWithUnits(std::string name, T val, std::string units):
	Variable<T>(name,val),
	unit(units){}
	~VariableWithUnits(){}
	
};


class ArrayOfVariables
{
	std::vector<VariableBase*> data;
public:
	template<typename T1>
	Variable<T1>* CreateAndAdd(std::string name, T1 val);
	void Print()const;
	ArrayOfVariables(const unsigned int size);
	~ArrayOfVariables(){}
	
};

typedef Variable<double> VariableDouble;
typedef Variable<int> VariableInt;
typedef Variable<char> VariableChar;
typedef Variable<bool> VariableBool;
typedef Variable<float> VariableFloat;

/////////////////
template<typename T>
void Variable<T>::Print()const{
	std::cout<<name<<": "<<value<<std::endl;
}
template<typename T >
void VariableWithUnits<T>::Print()const{
	std::cout<<Variable<T>::name<<": "<<Variable<T>::value<<" ["<<unit<<"]"<<std::endl;
}

ArrayOfVariables::ArrayOfVariables(const unsigned int size){
	
}
template<typename T1 > 
Variable<T1>* ArrayOfVariables::CreateAndAdd(std::string name, T1 val){
	Variable<T1>* temp = new Variable<T1>(name,val);
	data.push_back(temp);
	return temp;
	
}

void ArrayOfVariables::Print()const{
	for(int i=0;i<data.size();++i)
		data[i]->Print();
}