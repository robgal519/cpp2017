#pragma once
#include <iostream>

class IntSequence
{

	int tabela[50];
	int pozycja;
	int pojemnosc;
public:
	IntSequence();
	IntSequence(const IntSequence& ref);
	operator int();

	int operator()(); // zwraca element z aktualnej pozycji
	
	int operator++(int); // przesuwa index na n=kolejny element
	int operator--(int);// przesuwa index na poprzedni element

	IntSequence& operator<<(int val);// dodaje element do tabele
	void reset(); // teraz patrzymy na pierwszy elememt
	bool finished(); // zwraca 1 gry patrzymy na ostatni element

};