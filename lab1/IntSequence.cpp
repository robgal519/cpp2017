#include "IntSequence.h"

IntSequence::IntSequence():
pozycja(0),
pojemnosc(0)
{
	for (int i = 0; i < 50; ++i)
	{
		tabela[i]=0;
	}
}

IntSequence::IntSequence(const IntSequence& ref):
pozycja(ref.pozycja),
pojemnosc(ref.pojemnosc)
{
	for (int i = 0; i < 50; ++i)
	{
		tabela[i]=ref.tabela[i];
	}
}

IntSequence::operator int(){
	return tabela[pozycja%pojemnosc];
}

int IntSequence::operator()(){

	return tabela[pozycja%pojemnosc];
}

int IntSequence::operator++(int){

		pozycja++;

	return tabela[pozycja%pojemnosc];
}
int IntSequence::operator--(int){
	
		pozycja--;
	return tabela[pozycja%pojemnosc];
}
IntSequence& IntSequence::operator<<(int val){
	tabela[pojemnosc]=val;
	pojemnosc++;
	return *this;
}
void IntSequence::reset(){
	pozycja=0;
}

bool IntSequence::finished(){
	return ( pozycja < pojemnosc ? 0:1);
	
}