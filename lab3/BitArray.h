#pragma once

#include <iostream>
using std::ostream;
using std::cout;

class BoolType{
	unsigned int index_;
	unsigned char* data_;

public:
	BoolType(unsigned int index, unsigned char* table):index_(index),data_(table){}
	void operator=(bool value);
	operator bool();
};


class BitArray{
	unsigned char *data_;
	unsigned int volume_;
	unsigned int counterOfElements_;
public:

	BitArray(unsigned int size, bool value=false);
	~BitArray();

	//grant acces to single bit, which can be assigned, or read easily
	BoolType& operator[](unsigned int index);
	//convinient way to print content
	friend ostream& operator<<(ostream& cout, const BitArray& array);


};

ostream& operator<<(ostream& cout, const BitArray& array);
