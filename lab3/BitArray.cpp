#include "BitArray.h"

BitArray::BitArray(unsigned int size, bool value){
	if(size%sizeof(unsigned char))
		volume_=size/8+1;
	else
		volume_=size/8;


	data_ = new unsigned char[volume_];
	if(value)
	{
		for(int i=0;i<volume_;++i)
			data_[i]=255;
	}else
	{
		for(int i=0;i<volume_;++i)
			data_[i]=0;
	}
	counterOfElements_ = size;
}

BitArray::~BitArray(){
	delete[] data_;
}

BoolType& BitArray::operator[](unsigned int index){
	BoolType * temp = new BoolType(index, data_);
	return * temp;
}

///////////////////////////////////////

void BoolType::operator=(bool value){
	unsigned char mask8bit =128;
	mask8bit>>=index_/sizeof(unsigned char);

	if(value)
	{
		data_[index_%8] |= mask8bit;
	}
	else 
	{
		data_[index_%8] &= ~mask8bit;
	}

}

BoolType::operator bool(){
	unsigned char mask8bit =128;
	mask8bit>>=index_;

	return ((data_[index_%8] & mask8bit)? true:false);

}

void printNLastBits(unsigned char byte, int amount=8){


	for(int i=1;i<amount*8;i<<=1)
		if(byte & i)
			cout<<1;
		else
			cout<<0;

}

ostream& operator<<(ostream& cout, const BitArray& array){

int overhead = 8-((array.volume_*8)-array.counterOfElements_);


int i=(array.volume_)*8;
for(int j=array.volume_;j;j--)
{
	printNLastBits(array.data_[j],overhead);
	cout<<" ";
	overhead=8;
	i-=8;
}

}
