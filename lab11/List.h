#pragma once
#include <typeinfo>
#include <string>

class MasterNode
{
protected:
	MasterNode* next_; // pinter to next node
public:
	//simple getter
	MasterNode* next(){return next_;}

	MasterNode(MasterNode* ptr) :next_(ptr){};
	virtual ~MasterNode(); //destructor has to be virtual

	template<typename P>
	bool isType() const{ // more efficient to left inline, this function checks, if stored data,is type P trerurns 1, it true, 0 if false
		return Type(typeid(P).name());
	}
	//simple setter
	void setNext(MasterNode* ptr){next_ = ptr;}
	virtual bool Type(std::string) const = 0; 

};


template <typename T>
class Node : public MasterNode
{
	T data_;
public:
	
	Node(T data):data_(data), MasterNode(NULL){}
	
	~Node(){};
	virtual bool Type(std::string type) const;

	T data() const{return data_;}//simple so inline
};

class List
{
	MasterNode* head_;
public:
	MasterNode* head(){return head_;};
	List(): head_(NULL){}
	~List(){
if(head_)
	delete head_;
head_ = NULL;
	};

	template<typename C>
	static C get(MasterNode* ptr);

	template<typename K>
	void add(K val);
	
};

#include "List.cc"