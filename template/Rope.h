#pragma once
#include <iostream>
using namespace std;

template<typename T1, int s>
class Rope
{

class DadtaContainer
{
public:
	int index;
	DadtaContainer* next;
	T1 data[s];
	DadtaContainer();
	~DadtaContainer();
	void addToEnd(T1 val);	
};

DadtaContainer* head;
int counter;
int blockSize;
public:
	Rope();
	~Rope();
	Rope& operator,(T1 val);
	const T1& at(int index)const;
	int capacity()const;
	int size()const;
	friend ostream& operator<<(ostream cout, Rope r);
};
////////////////////////////////////////////////////////

ostream& operator<<(ostream cout, Rope r){
	for(int i=0;i<counter;++i)
		cout<< r.at(i);
	return cout;
}
int Rope::capacity()const{
	if(counter%blockSize)
		return counter/blockSize+1;
	return counter/blockSize;
}
int Rope::size()const{
	return counter;
}

const T1& Rope::at(int index)const{
	int block = index/blockSize;
	int id = index%blockSize;

DadtaContainer* temp = head;
	for (int i = 0; i < block; ++i)
	{
		temp = temp.next;
	}
return temp.data[id];
}

Rope& Rope::operator,(T1 val){
	head.addToEnd(val);
	counter++;
	return *this;
}

Rope::Rope(){
	head = new DadtaContainer();
	counter = 0;
	blockSize = s;
}

Rope::DadtaContainer::DadtaContainer(){
	next = NULL;
	if (size<0) throw -1;
	for(int i=0;i<size;++1)
		data[i]=0;
	index=0;
}

Rope::DadtaContainer::~DadtaContainer(){
	if(next)
		delete next;
}
Rope::~Rope(){
	delete head;
}

void Rope::DadtaContainer::addToEnd(T1 val){
	if(index<size)
	{
		data[index] = val;
		index ++;
	}
	else
	{
		next = new DadtaContainer();
		next.addToEnd(val);
	}
}