#include "SmartPointer.h"
#include "TestClassA.h"

SmartPointer::SmartPointer(TestClassA* ptr){
	ptr_ = ptr;
	counter = new int;
	(*counter)=1;
}

SmartPointer::~SmartPointer(){
	(*counter)--;

	if((*counter) == 0)
	{
		delete ptr_;
		delete counter;
	}
}

SmartPointer::SmartPointer(const SmartPointer& coppy){
	ptr_ = coppy.ptr_;
	counter = coppy.counter;
	(*counter)++;
}


void SmartPointer::operator=(const SmartPointer& pointer){
	if(this == &pointer)return;

	this->~SmartPointer();

	ptr_ = pointer.ptr_;
	counter = pointer.counter;

	(*counter)++;
}


const TestClassA& SmartPointer::operator*()const{
	return *ptr_;
}

const TestClassA* SmartPointer::operator->()const{
	return ptr_;
}
