#pragma once

class TestClassA;

class SmartPointer{
	TestClassA* ptr_;

 	int* counter;

public:
	//konstruktor
	SmartPointer(TestClassA* ptr);
	//konstruktor kopiujacy
	SmartPointer(const SmartPointer& copy);
	//zmniejsza licznik refgerencji, jeslicznik spadnie do 0 usuwa obiekt, na który wskazuje
	~SmartPointer();

	//wywoluje destruktor na this, a następnie wypelnia argumentem pointer
	void operator=(const SmartPointer& pointer );
	//zwraca referencję na obiekt który przechowuje
	const TestClassA& operator*()const ;
	//zwraca wskaznik na obiekt ktory przechowuje
	const TestClassA* operator->()const ;

};