#include "TestClassA.h"
#include <iostream>
using std::cout;
using std::endl;

TestClassA::TestClassA(string a):
name_(a)
{
	cout<<".. Konstruuje TestClassA "<<name_<<endl;
}

TestClassA::~TestClassA()
{
	cout<<".. Usuwam  TestClassA "<<name_<<endl;
}

string TestClassA::name()const
{
	return name_;
}