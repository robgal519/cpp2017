#pragma once
#include <string>
#include <iostream>
using std::string;
using std::cout;
using std::endl;

class TestClassA{
	string name_;
public:
	TestClassA(string a);
	~TestClassA();
	//prosty getter
	string name()const;

};