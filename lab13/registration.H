#pragma once

template< int id>
struct int2type
{
};



template<typename T>
struct type2int
{
};

#define DECLARE_TYPE_IDENTIFIER(Typ , val) template<>struct type2int<Typ>{\
const static int value=val;\
};\
template<>struct int2type<val>{\
typedef Typ type;\
};