#include "Logical.h"

bool Logical::eval(const DoubleOperation& oper, bool arg1, bool arg2)
{
	return oper.logicalOperation(arg1,arg2);
}
bool Logical::eval(const SingleOperation& oper, bool arg1)
{
	return oper.logicalOperation(arg1);
}