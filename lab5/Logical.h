#pragma once
// #include "Operation.h"
#include "Operators.h"

struct Logical
{	// DoubleOperations, and SingleOperation, are base clases, so I can store in them lower( in hierarchy) clases, and becouse they are abstract, i have to provide implementation for their virtual functions
	static bool eval(const DoubleOperation& oper, bool arg1, bool arg2);
	static bool eval(const SingleOperation& oper, bool arg1);
};