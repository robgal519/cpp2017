#pragma once
#include "Operation.h"


// Or expands class Doule Operations, and provide implementaition for its virtual function
class OR: public DoubleOperation{
public:
	// implementation of the virtual function, it does all the job in this excercise
	bool logicalOperation(bool arg1, bool arg2) const{
		//made it in line, becouse it is very simple, and it's faster that way, then calling seperate function
		return (arg1 or arg2);
	}
};

//above description complies to all classes in this file, each class provides implementation of difrent operation

class AND: public DoubleOperation{
public:

	bool logicalOperation(bool arg1, bool arg2) const{
		return (arg1 and arg2);
	}
};

class XOR: public DoubleOperation{
public:

	bool logicalOperation(bool arg1, bool arg2) const{
		return (arg1 xor arg2);
	}
};

class NOT: public SingleOperation{
public:

	bool logicalOperation(bool arg1) const{
		return (~arg1);
	}
};

class NOOP: public SingleOperation{
public:

	bool logicalOperation(bool arg1) const{
		return arg1;
	}
};