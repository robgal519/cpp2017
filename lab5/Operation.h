#pragma once
//base class for single operations
class SingleOperation{
public:
	//pure virtual function
	virtual bool logicalOperation(bool arg1)const = 0;
};

//similar as above, but for double operation functions

class DoubleOperation{
public:
	virtual bool logicalOperation(bool arg1, bool arg2)const = 0;
};

// i made that hierarchy becouse in natural way, operation like OR, AND are members of greater group of operations ( double argument Operations)
// the same reasons drive me to do similar structure for single argument operations